let userData = [];
let dataUserId = "";

$(document).ready(function() {
    $('#userTable').submit(function(event) {
        console.log(event);
        var formData = {
            'name'              : $('input[name=name]').val(),
            'email'             : $('input[name=email]').val(),
            'number'            : $('input[name=number]').val()
        };

        $.ajax({
            type        : 'POST',
            url         : 'https://reqres.in/api/users',
            data        : formData,
            dataType    : 'json', 
            encode          : true
        })
            .done(function(data) {

                
                console.log(data);

                userData.push(data);
                console.log(userData)
                appendToUsrTable(data);                
            })

            .fail(function(data) {
                $('form').html('<div class="alert alert-danger">Could not reach server, please try again later.</div>');
              });

        event.preventDefault();
    });
});

function appendToUsrTable(userData) {
    $("#employeeList > tbody:last-child").append(`
        <tr id="data-${userData.id}">
            <td class="userData" name="name">${userData.name}</td>
            '<td class="userData" name="email">${userData.email}</td>
            '<td class="userData" name="number">${userData.number}</td>
            '<td align="center">
                <button class="btn btn-success form-control" onClick="editUser(${userData.id})" data-toggle="modal" data-target="#myModal")">EDIT</button>
            </td>
            <td align="center">
                <button class="btn btn-danger form-control" onClick="deleteUser(${userData.id})">DELETE</button>
            </td>
        </tr>
    `);
}

function editUser(id) {
    userData.forEach(function(data, i) {
      if (data.id == id) {
        document.getElementById("f_name").value = data.name;
        document.getElementById("emailAddress").value = data.email;
        document.getElementById("phoneNumber").value = data.number;
        document.getElementById("submitDetails").remove();
        $(".footer").empty().append(`
                <button type="button" id=submitDetails onclick="updateUser(${id})">Save Changes</button>
        `);
        }
    });
   
}

function updateUser(id) {
    var formData = {
        'name'              : $('input[name=name]').val(),
        'email'             : $('input[name=email]').val(),
        'number'            : $('input[name=number]').val()
    };

    $.ajax({
        type        : 'PUT',
        url         : 'https://reqres.in/api/users',
        data        : formData,
        dataType    : 'json', 
        encode          : true
    })
        .done(function(data) {

            var user = {};
            user.id = id;
            let name = document.getElementById("f_name").value;
            let email = document.getElementById("emailAddress").value;
            let number = document.getElementById("phoneNumber").value;
            userData.forEach(function(user, i) {
              if (user.id == id) {
                user.name = name;
                user.email = email;
                user.number = number;
                $("#data-" + user.id).children(".userData").each(function() {
                  var attr = $(this).attr("name");
                  if (attr == "name") {
                    $(this).text(user.name);
                  } else if (attr == "email") {
                    $(this).text(user.email);
                  } else {
                    $(this).text(user.number);
                  }
                });
              }
            });
            
        })

        .fail(function(data) {
            $('form').html('<div class="alert alert-danger">Could not reach server, please try again later.</div>');
          });
}


function deleteUser(id) {

    var formData = {
        'name'              : $('input[name=name]').val(),
        'email'             : $('input[name=email]').val(),
        'number'            : $('input[name=number]').val()
    };

    $.ajax({
        type        : 'DELETE',
        url         : 'https://reqres.in/api/users',
        data        : formData,
        dataType    : 'json', 
        encode          : true
    })
        .done(function(data) {

            var action = confirm("Are you sure you want to delete this user?");
            userData.forEach(function(data, i) {
              if (data.id == id && action != false) {
                userData.splice(i, 1);
                $("#data-" + data.id).remove();
              }
            });
                           
        })

        .fail(function(data) {
            $('form').html('<div class="alert alert-danger">Could not reach server, please try again later.</div>');
          });
}